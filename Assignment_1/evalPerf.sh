#!/bin/sh
p=982451653
seed=300

echo "p: ${p}"
echo "seed: ${seed}"

export OMP_DYNAMIC=FALSE
export OMP_PROC_BIND=TRUE

for thread_num in 8 16 32; do
    printf "thread_num: ${thread_num} - N: 2000\n"
    export OMP_NUM_THREADS=$thread_num
    export GOMP_CPU_AFFINITY="256-$((256+(thread_num-1)))"
    for schedule in "static" "dynamic" "guided"; do
	printf "${schedule}: "
	for chunk_size in 2 4 8 16 32 64 128; do
	    export OMP_SCHEDULE="$schedule,$chunk_size"
	    for N in 2000; do
		for num in 1 2; do
		    ./gaussianElimParallelBasic $N $p $seed
		    if [ $num -lt 2 ]
		    then
			printf " / "
		    fi
		done
		printf " | "
	    done
	done
	printf "\n"
    done
done
