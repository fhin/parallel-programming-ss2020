#define ONE_SOLUTION 1
#define NO_SOLUTION -1
#define INF_SOLUTIONS 0

long* initArray(int N, uint seed, int p);
long parseArgVToLong(char* argv);
int longToInt(long val);
uint longToUInt(long val);
int verifySolution(long *reducedArr, long *solutionVec, int N);
void printArray(long* arr, int rows, int cols);
