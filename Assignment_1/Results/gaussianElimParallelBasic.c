#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <errno.h>
#include <omp.h>

#define ONE_SOLUTION 1
#define NO_SOLUTION -1
#define INF_SOLUTIONS 0

static long p = 982451653;
static long mAdd(long a, long b) { return (a+b)%p; }
static long mSub(long a,long b) {return (a+p-b)%p; }
static long mMul(long a, long b) {return (a*b)%p;}
static long mInv(long a){
  long r = p; long old_r = a;
  long s = 0; long old_s = 1;
  while (r != 0) {
    long q = old_r /r;
    long r0 = r; r = old_r-q*r; old_r = r0;
    long s0 = s; s = old_s-q*s; old_s = s0;
  }
  return old_s >= 0 ? old_s : old_s+p;
}
static long mDiv(long a, long b) {return mMul(a, mInv(b)); }

static long* initArray(int N, uint seed, int p){
  if (N <= 0 || p < 0) exit(EXIT_FAILURE);
  long* arr = calloc(N*(N+1), sizeof(long));
  if (arr == NULL) {
    exit(EXIT_FAILURE);
  }
  
  srandom(seed); //set seed for random number generator
  int idx = 0;
  for (int i = 0; i < N; i++){
    for (int j = 0; j < N+1; j++){
      // Create random number from range [0;p-1] as [M,n] => M + rand() / (RAND_MAX / (N - M + 1) + 1)
      arr[idx + j] = rand() / (RAND_MAX  / p + 1);
    }
    idx += N+1;
  }
  return arr;
}

static long parseArgVToLong(char* argv){
  char *endptr;
  char *str = argv;
  long argVal;
  errno = 0; /* To distringuish success/failure after call */
  argVal = strtol(argv, &endptr, 10);

  if ((errno == ERANGE && (argVal == LONG_MAX || argVal == LONG_MIN))
      || (errno != 0 && argVal == 0)) {
    // TODO: print error
    exit(EXIT_FAILURE);
  }
  if (endptr == str){
    // TODO: Print error no digits were found, see man page strtol
    exit(EXIT_FAILURE);
  }
  /* If we got here, strtol() successfully parsed a number */
  return argVal;
}

static int longToInt(long val){
  if (val <= INT_MIN || val >= INT_MAX){
    // TODO: Print error
    exit(EXIT_FAILURE);
  }
  return (int)val;
}

static uint longToUInt(long val){
  if (val <= INT_MIN || val >= INT_MAX){
    // TODO: Print error
    exit(EXIT_FAILURE);
  }
  return (uint)val;
}

int reduceEqtSystem(long* arr, long *solutionVec, int N){
  if (N <= 0) {
    return NO_SOLUTION;
  }
  int currRowIdx = 0;
  int numCols = N + 1;
  int maxRowStartIdx = (N-1)*numCols;
  int nextRowIdx, rowsToCheck;
  for (int currDiagIdx = 0; currDiagIdx < N; currDiagIdx++){
     // Diagonal element a[n,n] == 0
    nextRowIdx = currRowIdx + numCols; // idx = a[n+1, n]
    rowsToCheck = N - (currDiagIdx + 1);
    long currDiagElem = arr[currRowIdx + currDiagIdx];
    if (currDiagElem == 0){
      int replacementFound = 0;
      int nextRowElemIdx = nextRowIdx + currDiagIdx;
      while (rowsToCheck > 0) {
	if (arr[nextRowElemIdx] != 0){
	  long tmpVal = 0;
	  int currRowElemIdx = currRowIdx + currDiagIdx;
	  for (int elemsToSwap = currDiagIdx; elemsToSwap < N; elemsToSwap++){
	    tmpVal = arr[currRowElemIdx];
	    arr[currRowElemIdx++] = arr[nextRowElemIdx]; 
	    arr[nextRowElemIdx++] = tmpVal;
	  }
	  replacementFound = 1;
	  break;
	}
	rowsToCheck--;
      }
      if (replacementFound == 0){
	printf("No replacement element for pivot at idx %d\n", currDiagIdx);
	printf("Equation system has no unique solution\n");
	return NO_SOLUTION;
      }
    }
    long currRowElem, factor;
    rowsToCheck = N - (currDiagIdx + 1);
    int nextRowElemIdx;
    int pivotRowElemIdx = currRowIdx + currDiagIdx;
    #pragma omp parallel for firstprivate(pivotRowElemIdx) private(currRowElem, factor, nextRowElemIdx) schedule(static, 4)
    for (int assignedRow = 0; assignedRow < rowsToCheck; assignedRow++){
	nextRowElemIdx = nextRowIdx + currDiagIdx + (assignedRow * numCols);
	factor = mDiv(arr[nextRowElemIdx], currDiagElem);
	for (int currColFromDiag = 0; currColFromDiag < (numCols - currDiagIdx); currColFromDiag++){
	    currRowElem = arr[nextRowElemIdx];
	    arr[nextRowElemIdx++] = mSub(currRowElem, mMul(arr[pivotRowElemIdx + currColFromDiag], factor));
	}
	nextRowElemIdx -= numCols;
    }
    currRowIdx += numCols;
    maxRowStartIdx++;
  }
  int padding=0;
  int numRowsToParse = N-1;
  int startIdx = N*numCols - (padding+2);
  currRowIdx = 0;
  long currSolutionVecElem;
  solutionVec[numRowsToParse] = arr[startIdx+1];
  startIdx -= numCols;
  for (int i = 0; i < N; i++){
    if (numRowsToParse > 0){
      solutionVec[numRowsToParse-1] = mAdd(solutionVec[numRowsToParse-1], arr[startIdx+1]);
    }
    if (arr[startIdx+numCols-i] == 0){
      return NO_SOLUTION;
    }
    if (solutionVec[numRowsToParse] == 0){
      return INF_SOLUTIONS;
    }
    currSolutionVecElem = mDiv(solutionVec[numRowsToParse], arr[startIdx+numCols-i]);
    solutionVec[numRowsToParse] = currSolutionVecElem;
      
    #pragma omp parallel for private(currRowIdx) schedule(guided)
    for(int j = 0; j < numRowsToParse; j++){
      currRowIdx = (j+1)*numCols - padding - (i+2);
      solutionVec[j] = mSub(solutionVec[j], mMul(arr[currRowIdx], currSolutionVecElem));
    }
    numRowsToParse--;
    startIdx -= numCols;
  }
  return ONE_SOLUTION;
}

int verifySolution(long *reducedArr, long *solutionVec, int N, int padding){
  if (reducedArr == NULL || solutionVec == NULL || N <= 0 || padding < 0){
    exit(EXIT_FAILURE);
  }
  long lSideSum = 0;
  long rSideSum = 0;
  int currRowStartIdx = 0;
  int currElemIdx = 0;
  int isSolutionSound = 1;
  for (int i = 0; i < N; i++){
    lSideSum = 0;
    rSideSum = reducedArr[currRowStartIdx + (N-i)];
    currElemIdx = currRowStartIdx;
    for (int j = i; j < N; j++){
      lSideSum = mAdd(lSideSum, mMul(solutionVec[j], reducedArr[currElemIdx++]));
    }
    if (lSideSum != rSideSum){
      isSolutionSound = 0;
      break;
    }
    currRowStartIdx += (N+2)+padding;
  }
  return isSolutionSound;
}

void printArray(long* arr, int rows, int cols){
  for (int row = 0; row < rows; row++){
    int currRowIdx = row * cols;
    for (int col = 0; col < cols; col++){
      printf("| %ld ", arr[currRowIdx + col]);
    }
    printf("\n");
  }
}

int main(int argc, char* argv[]){
  if (argc != 4) {
    printf("Expected parameters: <size_of_matrix> <prime_number> <seed>\n");
    exit(EXIT_FAILURE);
  }
  uint seed = longToUInt(parseArgVToLong(argv[3]));
  p = parseArgVToLong(argv[2]);
  int N = longToInt(parseArgVToLong(argv[1]));
  //printf("N: %d\n", N);
  //printf("p: %ld\n", p);
  //printf("seed: %d\n", seed);
  
  long *arr = initArray(N, seed, p);
  long *solutionVec = calloc(N, sizeof(long));
  if (arr == NULL || solutionVec == NULL){
    exit(EXIT_FAILURE);
  }
  //printf("Current arr: \n");
  //printArray(arr, N,N+1);
  
  double tic = omp_get_wtime();
  double toc;
  int res = reduceEqtSystem(arr, solutionVec, N);
  toc = omp_get_wtime();
  printf("Time [s]: %f\n", toc - tic);
  
  
  //printf("Reduced arr: \n");
  //printArray(arr, N, N+9);

  if (res == ONE_SOLUTION){
    printf("Found solution for equation system\n");
    //printf("Solution: \n");
    //printArray(solutionVec, 1, N);
    printf("Verifying solution...\n");
    res = verifySolution(arr, solutionVec, N, 0);
    if (res == 1){
      printf("Solution is correct !\n");
    }
    else {
      printf("Solution is incorrect !\n");
    }
  }
  else {
    printf("Error reducing equation system\n");
    if(res == INF_SOLUTIONS){
      printf("Equation has infinitly many solutions\n");
    }
    else {
      printf("Equation has no solution\n");
    }
  }
  
  free(solutionVec);
  free(arr);
  exit(EXIT_SUCCESS);
}
