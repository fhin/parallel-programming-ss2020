#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <errno.h>
#include <omp.h>

#define ONE_SOLUTION 1
#define NO_SOLUTION -1
#define INF_SOLUTIONS 0

static long p = 982451653;
static long mAdd(long a, long b) { return (a+b)%p; }
static long mSub(long a,long b) {return (a+p-b)%p; }
static long mMul(long a, long b) {return (a*b)%p;}
static long mInv(long a){
  long r = p; long old_r = a;
  long s = 0; long old_s = 1;
  while (r != 0) {
    long q = old_r /r;
    long r0 = r; r = old_r-q*r; old_r = r0;
    long s0 = s; s = old_s-q*s; old_s = s0;
  }
  return old_s >= 0 ? old_s : old_s+p;
}
static long mDiv(long a, long b) {return mMul(a, mInv(b)); }

static long* initArray(int N, uint seed, int p){
  if (N <= 0 || p < 0) exit(EXIT_FAILURE);
  long* arr = calloc(N*(N+9), sizeof(long));
  if (arr == NULL) {
    exit(EXIT_FAILURE);
  }
  
  srandom(seed); //set seed for random number generator
  int idx = 0;
  for (int i = 0; i < N; i++){
    for (int j = 0; j < N+1; j++){
      // Create random number from range [0;p-1] as [M,n] => M + rand() / (RAND_MAX / (N - M + 1) + 1)
      arr[idx + j] = rand() / (RAND_MAX  / p + 1);
      //arr[idx + j] = i*j + i;
    }
    idx += N+9;
  }
  return arr;
}

static long parseArgVToLong(char* argv){
  char *endptr;
  char *str = argv;
  long argVal;
  errno = 0; /* To distringuish success/failure after call */
  argVal = strtol(argv, &endptr, 10);

  if ((errno == ERANGE && (argVal == LONG_MAX || argVal == LONG_MIN))
      || (errno != 0 && argVal == 0)) {
    // TODO: print error
    exit(EXIT_FAILURE);
  }
  if (endptr == str){
    // TODO: Print error no digits were found, see man page strtol
    exit(EXIT_FAILURE);
  }
  /* If we got here, strtol() successfully parsed a number */
  return argVal;
}

static int longToInt(long val){
  if (val <= INT_MIN || val >= INT_MAX){
    // TODO: Print error
    exit(EXIT_FAILURE);
  }
  return (int)val;
}

static uint longToUInt(long val){
  if (val <= INT_MIN || val >= INT_MAX){
    // TODO: Print error
    exit(EXIT_FAILURE);
  }
  return (uint)val;
}

void printArray(long* arr, int rows, int cols){
  for (int row = 0; row < rows; row++){
    int currRowIdx = row * cols;
    for (int col = 0; col < cols; col++){
      printf("| %ld ", arr[currRowIdx + col]);
    }
    printf("\n");
  }
}

int reduceEqtSystem(long* arr, long* solutionVec, int N){
  if (N <= 0) {
    return -1;
  }
  int padding = 8;
  int numCols = (N+1) + padding;
  int maxNumThreads = omp_get_max_threads();
  int numThreads = maxNumThreads; // TODO: Update to use the correct amount of threads so that every thread has rows to process
  if ((N-1) < maxNumThreads){
    numThreads = (N-1);
  }
  int rowsPerThread = (int)((N-1) / numThreads);
  int leftOverRows = (N-1) % numThreads;
  int currRowCnter = 1;
  int numColsToParse = numCols - 8;
  long *threadRowLookupMat[N];
  int replacementFound = -1;
  int changedRowIdx = -1;
  int reductionResult = ONE_SOLUTION;
  if (threadRowLookupMat == NULL){
    exit(EXIT_FAILURE);
  }
   threadRowLookupMat[0] = arr;
  #pragma omp parallel firstprivate(numColsToParse) num_threads(numThreads)
  {
    int currRow;
    #pragma omp critical (startRowAssignment)
    {
      currRow = currRowCnter++;
    }
    int minRowIdx = currRow;
    int startRowVal = currRow;
    int numAssignedRows = rowsPerThread;
    int skipRowCnt = 0;
    int replacementNeeded = 0;
    long *currPivotRowAdr;
    long currDiagElem;
    if (leftOverRows > 0 && currRow <= leftOverRows){
      numAssignedRows++;
    }
    //long *(*assignedRowAdresses) = calloc(numAssignedRows, sizeof(long*));
    long *assignedRowAdresses[numAssignedRows];
    // Add table row adress to lookup table
    for (int numParsedRows = 0; numParsedRows < numAssignedRows; numParsedRows++){
      long *matRowAdr = (long*) &(arr[currRow * numCols]);
      threadRowLookupMat[currRow] = matRowAdr;
      assignedRowAdresses[numParsedRows] = matRowAdr;
      currRow += numThreads;
    }
    #pragma omp barrier
    for (int currDiagIdx = 0; currDiagIdx < N; currDiagIdx++){
      currPivotRowAdr = threadRowLookupMat[currDiagIdx] + currDiagIdx;
      currDiagElem = *currPivotRowAdr;
      #pragma omp barrier
      if (minRowIdx <= currDiagIdx){
	if (skipRowCnt < numAssignedRows){
	  skipRowCnt++;
	  minRowIdx += maxNumThreads;
	}
      }
      if (currDiagElem == 0){
	replacementNeeded = 1;
	if (replacementFound == -1){
	  replacementFound = 0;
	}
	for (int numParsedRows = skipRowCnt; numParsedRows < numAssignedRows; numParsedRows++){
	  if (*(assignedRowAdresses[numParsedRows] + currDiagIdx) != 0){
            #pragma omp critical (exchangeRowCrit)
	    {
	      if (replacementFound != 1){
		replacementFound = 1;
		changedRowIdx = numParsedRows * maxNumThreads + startRowVal;
		long tVal = 0;
		long *replacementRowAdr = threadRowLookupMat[changedRowIdx];
		for (int idx = currDiagIdx; idx < (N+1); idx++){
		  tVal = *(currPivotRowAdr + idx);
		  *(currPivotRowAdr + idx) = *(replacementRowAdr + idx);
		  *(replacementRowAdr + idx) = tVal;
		}
	      }
	    }
	    break;
	  }
	}
      }
      if (replacementNeeded == 1){
	#pragma omp barrier
	if (replacementFound == 0){
	  printf("No replacement element found for pivot element at idx %d\n", currDiagIdx);
	  reductionResult = NO_SOLUTION;
	  break;
	}
	else {
	  replacementNeeded = 0;
	}
      }
      replacementFound = -1;
      long currDiagElem = *currPivotRowAdr;
      long factor;
      long currRowElem = 0;
      long *currRowAdr;
     
      for (int numParsedRows = skipRowCnt; numParsedRows < numAssignedRows; numParsedRows++){
	currRowAdr = assignedRowAdresses[numParsedRows] + currDiagIdx;
	factor = mDiv(*currRowAdr, currDiagElem);
	for (int numPCols = 0; numPCols < numColsToParse; numPCols++){
	  currRowElem = *(currRowAdr);
	  *(currRowAdr++) = mSub(currRowElem, mMul(*(currPivotRowAdr + numPCols), factor));
	}
      }
      numColsToParse--;
    }
  }
  if (reductionResult == NO_SOLUTION){
    printf("Equation system has no unique solution\n");
  }
  else {
    int numRowsToParse = N-1;
    int startIdx = N*numCols - (padding+2);
    int currRowIdx;
    long currSolutionVecElem;
    solutionVec[numRowsToParse] = arr[startIdx+1];
    startIdx -= numCols;
    for (int i = 0; i < N; i++){
      if (numRowsToParse > 0){
        solutionVec[numRowsToParse-1] = mAdd(solutionVec[numRowsToParse-1], arr[startIdx+1]);
      }
      if (arr[startIdx+numCols-i] == 0){
	reductionResult = NO_SOLUTION;
	break;
      }
      if (solutionVec[numRowsToParse] == 0){
	reductionResult = INF_SOLUTIONS;
	break;
      }
      currSolutionVecElem = mDiv(solutionVec[numRowsToParse], arr[startIdx+numCols-i]);
      solutionVec[numRowsToParse] = currSolutionVecElem;
      
      #pragma omp parallel for private(currRowIdx) schedule(guided)
      for(int j = 0; j < numRowsToParse; j++){
	currRowIdx = (j+1)*numCols - padding - (i+2);
	solutionVec[j] = mSub(solutionVec[j], mMul(arr[currRowIdx], currSolutionVecElem));
      }
      numRowsToParse--;
      startIdx -= numCols;
    }
  }
  return reductionResult;
}

int verifySolution(long *reducedArr, long *solutionVec, int N, int padding){
  if (reducedArr == NULL || solutionVec == NULL || N <= 0 || padding < 0){
    exit(EXIT_FAILURE);
  }
  long lSideSum = 0;
  long rSideSum = 0;
  int currRowStartIdx = 0;
  int currElemIdx = 0;
  int isSolutionSound = 1;
  for (int i = 0; i < N; i++){
    lSideSum = 0;
    rSideSum = reducedArr[currRowStartIdx + (N-i)];
    currElemIdx = currRowStartIdx;
    for (int j = i; j < N; j++){
      lSideSum = mAdd(lSideSum, mMul(solutionVec[j], reducedArr[currElemIdx++]));
    }
    if (lSideSum != rSideSum){
      isSolutionSound = 0;
      break;
    }
    currRowStartIdx += (N+2)+padding;
  }
  return isSolutionSound;
}

int main(int argc, char* argv[]){
  if (argc != 4) {
    printf("Expected parameters: <size_of_matrix> <prime_number> <seed>\n");
    exit(EXIT_FAILURE);
  }
  uint seed = longToUInt(parseArgVToLong(argv[3]));
  p = parseArgVToLong(argv[2]);
  int N = longToInt(parseArgVToLong(argv[1]));
  //printf("N: %d\n", N);
  //printf("p: %ld\n", p);
  //printf("seed: %d\n", seed);
  
  long *arr = initArray(N, seed, p);
  long *solutionVec = calloc(N, sizeof(long));
  if (arr == NULL || solutionVec == NULL){
    exit(EXIT_FAILURE);
  }
  //printf("Current arr: \n");
  //printArray(arr, N,N+9);

  double tic = omp_get_wtime();
  double toc;
  int res = reduceEqtSystem(arr, solutionVec, N);
  toc = omp_get_wtime();
  printf("Time [s]: %f\n", toc - tic);
  
  //printf("Reduced arr: \n");
  //printArray(arr, N, N+9);

  if (res == ONE_SOLUTION){
    printf("Found solution for equation system\n");
    //printf("Solution: \n");
    //printArray(solutionVec, 1, N);
    printf("Verifying solution...\n");
    res = verifySolution(arr, solutionVec, N, 8);
    if (res == 1){
      printf("Solution is correct !\n");
    }
    else {
      printf("Solution is incorrect !\n");
    }
  }
  else {
    printf("Error reducing equation system\n");
    if(res == INF_SOLUTIONS){
      printf("Equation has infinitly many solutions\n");
    }
    else {
      printf("Equation has no solution\n");
    }
  }
  
  free(solutionVec);
  free(arr);
  exit(EXIT_SUCCESS);
}
