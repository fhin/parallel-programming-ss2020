#!/bin/sh
p=982451653
seed=300

echo "p: ${p}"
echo "seed: ${seed}"

export OMP_DYNAMIC=FALSE
export OMP_PROC_BIND=TRUE
for i in 1 2 4 8 16 32; do
	export OMP_NUM_THREADS=$i
	export GOMP_CPU_AFFINITY="256-$((256+(i-1)))"
	echo "num threads: ${i}"
        for N in 2000 3350; do
		echo "N: ${N}"
		for num_run in 1 2 3 4 5; do
			echo "i: ${num_run}"
	                	./gaussianElimParallelBasic $N $p $seed
		done
        done
done

