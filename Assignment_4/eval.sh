#!/bin/bash
NUM_RUNS=5
IS_SERVER=0
if [ $# -eq 0 ] || [ $# -gt 3 ]; then
	echo "Use script as: ./eval.sh <source_file.c> <is_server> <num_runs>"
	exit 1
elif [ $# -eq 3 ]; then
	NUM_RUNS=$3
fi
if [ $# -gt 1 ]; then
    if [ $2 -eq 1 ]; then
	module load GnuCC/7.2.0
	module load mpt
	IS_SERVER=1
    fi
fi
SRC_FILE=$1
SRC_FILE_NAME=$(basename "$SRC_FILE" .c)

if [ ! -f ./"$SRC_FILE" ]; then
    echo "Source file does not exist"
    exit 1
fi

echo "Running benchmark with file: $SRC_FILE"
echo "Running benchmark $NUM_RUNS iterations"
LOG_FILE="log_${SRC_FILE_NAME}.txt"
RES_LOG_FILE="log_${SRC_FILE_NAME}_results.txt"
tmp_f="tmp_f.txt"

if [ ! -f ./"$tmp_f" ]; then
	touch ./"$tmp_f"
else
	> ./"$tmp_f"
fi

echo "Storing complete output in log file: $LOG_FILE"
if [ ! -f ./"$LOG_FILE" ]; then
	touch ./"$LOG_FILE"
else
	> ./"$LOG_FILE"
fi

echo "Storing metrics in: $RES_LOG_FILE"
if [ ! -f ./"$RES_LOG_FILE" ]; then
	touch ./"$RES_LOG_FILE"
else
	> ./"$RES_LOG_FILE"
fi

p=98245163
#p=4001
seed=300
padding=8
echo "P: ${p}"
echo "seed: ${seed}"
echo "Padding: ${padding}"
#MATRIX_SIZEs=(2200 3400)
# IF max(num_threads)=64 then kgV or lcm = 192 else 96

make
echo ""
for i in 1 2 4 8 12 16 24 32 48 64
do
    if [ $IS_SERVER -eq 1 ] && [ $i -gt 1 ]; then
	export MPI_DSM_CPULIST="0-$((i-1))"
    fi
	echo "Using $i Threads" | tee -a ./"$RES_LOG_FILE" ./"$LOG_FILE"
	for mat_size in 2112 3264
	do
	    > ./"$tmp_f"
	    echo "Using matrix size: $mat_size" | tee -a ./"$RES_LOG_FILE" ./"$LOG_FILE"
	    for j in $(seq $NUM_RUNS);
	    do
		mpirun -np $i ./"$SRC_FILE_NAME" $mat_size $p $seed $padding | tee -a ./"$tmp_f" ./"$LOG_FILE" > /dev/null
		grep -E '(Elapsed time \[in seconds\]: )([0-9]+).([0-9]+)\s([a-z]+)' ./"$tmp_f" >> ./"$RES_LOG_FILE"
		> ./"$tmp_f"
	    done
	    echo "" >> ./"$RES_LOG_FILE"
		echo "" >> ./"$LOG_FILE"
	done
	> ./"$tmp_f"
echo "" >> ./"$RES_LOG_FILE"
echo "" >> ./"$LOG_FILE"
done
rm -f ./"$tmp_f"

