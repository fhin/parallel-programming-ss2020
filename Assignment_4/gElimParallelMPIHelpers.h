long* initArray(int N, uint seed, int p, int padding);
long parseArgVToLong(char* argv);
int longToInt(long val);
uint longToUInt(long val);
void printArray(long *arr, int rows, int cols);
