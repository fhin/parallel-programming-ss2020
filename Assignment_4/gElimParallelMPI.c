#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <time.h>
#include "mpi.h"
#include "gElimParallelMPIHelpers.h"
// For debugging see: open-mpi.org/faq/?category=debugging
// mpirun -np <NP> xterm -e gdb program
// mpirun -n <NP> xterm -hold -e gdb -ex run --args ./program [arg1] [...]

#define ONE_SOLUTION 1
#define NO_SOLUTION 0
#define INF_SOLUTIONS -1
#define MASTER_RANK 0

typedef struct backSub_S {
  int currSubElemOK;
  long currSubElem;
} backSubHelper;

static long p = 982451653;
static long mAdd(long a, long b) { return (a+b)%p; }
static long mSub(long a,long b) {return (a+p-b)%p; }
static long mMul(long a, long b) {return (a*b)%p;}
static long mInv(long a){
  long r = p; long old_r = a;
  long s = 0; long old_s = 1;
  while (r != 0) {
    long q = old_r /r;
    long r0 = r; r = old_r-q*r; old_r = r0;
    long s0 = s; s = old_s-q*s; old_s = s0;
  }
  return old_s >= 0 ? old_s : old_s+p;
}
static long mDiv(long a, long b) {return mMul(a, mInv(b)); }

int reduceEqtSystem(long *arr, long *pivotRowArr, int N, int numAssignedRows, int padding, int taskId, MPI_Comm *comm){
  if (!arr || !pivotRowArr || N <= 0 || numAssignedRows <= 0 || padding < 0 || taskId < 0){
    return NO_SOLUTION;
  }
  int numCols = N+1+padding;
  int numColsToCheck = N+1;
  int minRowNum = taskId;
  int minRowIdx = 0;
  int rowDistance = N / numAssignedRows;
  long *currRowAdr = (long *) &arr[0];
  long *currPivotAdr = (long *) &pivotRowArr[0];
  for (int currDiagIdx = 0; currDiagIdx < N; currDiagIdx++){
    if (currDiagIdx >= minRowNum){
      minRowNum += rowDistance;
      minRowIdx++;
      currRowAdr += numCols;
    }
    
    if (currDiagIdx % rowDistance == taskId){
      // Update content of pivot row to be distributed, copy N-i elements instead of whole row
      memcpy(currPivotAdr, (long *) &arr[(minRowIdx-1) * numCols + currDiagIdx], numColsToCheck * sizeof(long));
    }
    // Broadcast current pivot row, again only copy N-i elements
    MPI_Bcast(currPivotAdr, numColsToCheck, MPI_LONG, currDiagIdx % rowDistance, *comm);
    if (!*currPivotAdr)
      return INF_SOLUTIONS;
    
    if (minRowIdx < numAssignedRows){
      long *currRowAdrBackup = (long *) &(*currRowAdr);
      for (int currRowIdx = minRowIdx; currRowIdx < numAssignedRows; currRowIdx++){
	// factor = A[j, i] / A[i, i];
	long factor = mDiv(*currRowAdr, *currPivotAdr);
	for (int currPivotRelIdx = 0; currPivotRelIdx < numColsToCheck; currPivotRelIdx++){
	  *currRowAdr = mSub(*currRowAdr, mMul(*currPivotAdr++, factor));
	  currRowAdr++;
	}
	// Move to first element of next assigned row
	currRowAdr += padding + currDiagIdx;
	// Reset to first element [at position of current diagonal index] of pivot row
	currPivotAdr -= (N+1) - currDiagIdx;
      }
      currRowAdr = currRowAdrBackup;
      currRowAdr++;
      currPivotAdr++;
    }
    numColsToCheck--;
  }
  return ONE_SOLUTION;
}

int solveEqts(long *assignedArr, long *solutionVec, int taskId, int N, int numAssignedRows, int padding, MPI_Datatype *backSubHelperType, MPI_Comm *comm){
  if (taskId < 0 || N <= 0 || assignedArr == NULL){
    return NO_SOLUTION;
  }
  backSubHelper backSubStruct;
  backSubStruct.currSubElemOK = ONE_SOLUTION;
  backSubStruct.currSubElem = 0;
  
  int currRowNum = numAssignedRows-1;
  int globalRowNum = N-1;
  int numCols = N+1+padding;
  int numThreads = N / numAssignedRows;
  /* Task 0 is assigned rows 0, P, 2P, ...,  n*P
     Task 1 is assigned rows 1, P+1, 2P+1, ...,  N*P +1 => maxRowIdx = P * (n-1) + 1
   */
  //int maxRowNum = taskId + (numAssignedRows-1) * numAssignedRows;
  /*
    0 - 0 3 6    0 3    0 3 6 9  12
    1 - 1 4 7    1 4    1 4 7 10 13
    2 - 2 5 8    2 5    2 5 8 11 14
   */
  int maxRowNum = taskId + numThreads * (numAssignedRows-1);
  int currSubRowIdx = (numAssignedRows-1) * numCols + (numCols - padding - 2);
  long currSubElem;

  // Copy b-vector into solutionVec
  int tmpRowIdx = taskId;
  int tmpbVecIdx = N;
  for (int i = 0; i < numAssignedRows; i++){
    // x[i] = b[i]
    solutionVec[tmpRowIdx] = assignedArr[tmpbVecIdx];
    tmpRowIdx += numThreads;
    tmpbVecIdx += numCols;
  }
  while (globalRowNum >= 0) {
    // Calculate new variable x[i] to substitute
    if (globalRowNum == maxRowNum){
      currSubElem = assignedArr[currSubRowIdx];
      if (!solutionVec[maxRowNum]) {
	backSubStruct.currSubElemOK = INF_SOLUTIONS;
      }
      else {
	currSubElem = mDiv(solutionVec[maxRowNum], currSubElem);
	backSubStruct.currSubElem = currSubElem;
	backSubStruct.currSubElemOK = currSubElem != 0;
      }
    }
    // Distribute x[i] to all workers
    MPI_Bcast(&backSubStruct, 1, *backSubHelperType, globalRowNum % numThreads, *comm);
  
    if (backSubStruct.currSubElemOK)
      currSubElem = backSubStruct.currSubElem;
    else
      return backSubStruct.currSubElemOK;
    
    if (taskId == MASTER_RANK){
      solutionVec[globalRowNum] = currSubElem;
    }
    if (globalRowNum <= maxRowNum){
      currRowNum--;
      currSubRowIdx -= numCols;
      maxRowNum -= numThreads;
    }
    // Update rows by plugging in the current variable
    int currSubElemIdx = currSubRowIdx;
    int tmpMaxRowNum = maxRowNum;
    
    if (currRowNum >= 0 && globalRowNum > 0) {
      for (int rowNum = currRowNum; rowNum >= 0; rowNum--){
	// x[j] -= x[i] * A[j, N-i]
	solutionVec[tmpMaxRowNum] = mSub(solutionVec[tmpMaxRowNum], mMul(assignedArr[currSubElemIdx], currSubElem));
	currSubElemIdx -= numCols;
	tmpMaxRowNum -= numThreads;
      }
    }
    currSubRowIdx--;
    globalRowNum--;
  }
  return ONE_SOLUTION;
}

void freeThreadMem(long *assignedArr, long *pivotRowArr){
  if (!assignedArr){
    free(assignedArr);
  }
  if (!pivotRowArr){
    free(pivotRowArr);
  }
}

// Verify solution by plugging in the values of the solution vector x in the reduced system to check if A'*x = b'
int verifySolution(long *reducedArr, long *solutionVec, int N, int numAssignedRows, int taskId, int padding, MPI_Comm *comm){
  if (!reducedArr || !solutionVec || numAssignedRows <= 0 || padding < 0 || !comm || !(*comm)){
    return NO_SOLUTION;
  }
  long lSideSum = 0;
  long rSideSum = 0;
  int currElemIdx = taskId;
  int rowDistance = N / numAssignedRows;
  int currRowNum = taskId;
  for (int i = 0; i < numAssignedRows; i++){
    for (int j = 0; j < N-currRowNum; j++){
      lSideSum = mAdd(lSideSum, mMul(solutionVec[currRowNum+j], reducedArr[currElemIdx++]));
    }
    rSideSum = reducedArr[currElemIdx];
    if (lSideSum != rSideSum){
      return NO_SOLUTION;
    }
    currRowNum += rowDistance;
    currElemIdx += currRowNum + 1 + padding;
    lSideSum = 0;
    rSideSum = 0;
  }
  return ONE_SOLUTION;
}

int main(int argc, char* argv[]){
  if (argc < 4 || argc > 6) {
    printf("Expected parameters: <size_of_matrix> <prime_number> <seed> <padding> [<print>]\n");
    exit(EXIT_FAILURE);
  }
  uint seed = longToUInt(parseArgVToLong(argv[3]));
  p = parseArgVToLong(argv[2]);
  int padding = argc > 4 ? longToInt(parseArgVToLong(argv[4])) : 0;
  if (padding < 0){
    printf("Padding cannot be smaller than 0");
    exit(EXIT_FAILURE);
  }
  int print = argc > 5 ? longToInt(parseArgVToLong(argv[5])) : 0;
  int N = longToInt(parseArgVToLong(argv[1]));
  int initArrCode = 1;

  MPI_Comm comm = MPI_COMM_WORLD;
  int numTasks, rank, numAssignedElems, rowsPerThread;
  long *arr, *assignedArr, *pivotRowArr;
  int numCols = N+1 + padding;
  
  MPI_Init(&argc, &argv);     /* Init MPI */
  MPI_Comm_size(comm, &numTasks); /* Get num threads */
  MPI_Comm_rank(comm, &rank); /* Get thread id */

  if (print && rank == MASTER_RANK){
    printf("N: %d\n", N);
    printf("p: %ld\n", p);
    printf("seed: %d\n", seed);
  }
  
  if (numTasks > N || N % (N / numTasks) != 0){
    if (rank == MASTER_RANK)
      printf("Matrix size is not evenly dividable by the number of assigned workers\n");
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }
  rowsPerThread = N / numTasks;
  numAssignedElems = (int) rowsPerThread * numCols;
  if (rank == MASTER_RANK){
    arr = initArray(N, seed, p, padding);
    if (!arr){
      initArrCode = 0;
    }
    else
      if (print) {
	printf("Current array:\n");
	printArray(arr,N,N+padding);
      }
  }
  MPI_Allreduce(&initArrCode, &initArrCode, 1, MPI_INT, MPI_BAND, comm);
  if (!initArrCode){
    MPI_Finalize();
    if (rank == MASTER_RANK)
      exit(EXIT_FAILURE);
  }
  int threadResOK = 1;
  assignedArr = calloc(numAssignedElems, sizeof *assignedArr);
  if (!assignedArr){
    initArrCode = 0;
    threadResOK = 0;
  }
  else {
    pivotRowArr = calloc(numCols - padding, sizeof *pivotRowArr);
    if (!pivotRowArr){
      initArrCode = 0;
      threadResOK = -1;
    }
  }
  MPI_Allreduce(&initArrCode, &initArrCode, 1, MPI_INT, MPI_BAND, comm);  
  // Check if all buffers are created correctly, abort and free allocated memory if not
  if (!initArrCode){
    if (threadResOK < 1) {
      if (!threadResOK)
	free(assignedArr);
      else
	free(pivotRowArr);
    }
    else
      freeThreadMem(assignedArr, pivotRowArr);
    if (rank == MASTER_RANK){
      free(arr);
    }
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }
  // All buffers created correctly
  MPI_Datatype backSubType;
  int num_items = 2;
  int blockSizePerElem[] = {1,1}; // Number of elements per block
  MPI_Datatype types[] = {MPI_INT, MPI_LONG};
  MPI_Aint offsets[] = {offsetof(struct backSub_S, currSubElemOK), offsetof(struct backSub_S, currSubElem)};
  MPI_Type_create_struct(num_items, blockSizePerElem, offsets, types, &backSubType);
  MPI_Type_commit(&backSubType);
  
  // Distribute array in round robin fashion to threads
  int blockSize = (N/rowsPerThread) * numCols;
  int currBlockIdx = 0;
  int currArrIdx = 0;
  MPI_Datatype array_type;
  MPI_Type_contiguous(numCols, MPI_LONG, &array_type);
  MPI_Type_commit(&array_type);

  for (int blockIdx = 0; blockIdx < N/numTasks; blockIdx++){
    MPI_Scatter(&arr[currBlockIdx], 1, array_type, &assignedArr[currArrIdx], 1, array_type, MASTER_RANK, comm);
    currArrIdx += numCols;
    currBlockIdx += blockSize;
  }
  double tic = 0;
  double toc = 0;
  if (rank == MASTER_RANK){
    tic = MPI_Wtime();
  }
  int res = reduceEqtSystem(assignedArr, pivotRowArr, N, rowsPerThread, padding, rank, &comm);
  if (res == ONE_SOLUTION){
    if (print){
      printf("Reduced array:\n");
      printArray(assignedArr, N/numTasks, N+1+padding);
    }
    
    res = solveEqts(assignedArr, pivotRowArr, rank, N, rowsPerThread, padding, &backSubType, &comm);
    
    if (res == ONE_SOLUTION){
      if (rank == MASTER_RANK){
	printf("Found solution for equation system\n");
	if (print){
	  printf("Solution: \n");
	  printArray(pivotRowArr, 1, N);
	}
	printf("Verifying solution...\n");
      }
      MPI_Bcast(pivotRowArr, N, MPI_LONG, MASTER_RANK, comm);
      res = verifySolution(assignedArr, pivotRowArr, N,rowsPerThread, rank, padding, &comm);
      MPI_Reduce(rank == MASTER_RANK ? MPI_IN_PLACE : &res, &res, 1, MPI_INT, MPI_BAND, MASTER_RANK, comm);
      if (rank == MASTER_RANK){
	toc = MPI_Wtime();
	if (res == 1){
	  printf("Solution is correct !\n");
	}
	else {
	  printf("Solution is incorrect !\n");
	}
      }
    }
    else {
      if (rank == MASTER_RANK){
	printf("Error reducing equation system\n");
	if(res == INF_SOLUTIONS){
	  printf("Equation has infinitly many solutions\n");
	}
	else {
	  printf("Equation has no solution\n");
	}
      }
    }
  }
  else {
    if (rank == MASTER_RANK) {
      toc = MPI_Wtime();
      printf("Error reducing equation system !\n");
    }
  }
  if (rank == MASTER_RANK){
    printf("Elapsed time [in seconds]: %f s\n", toc - tic);
  }
  // Free all allocated arrays and created MPI data types
  MPI_Type_free(&backSubType);
  MPI_Type_free(&array_type);
  freeThreadMem(assignedArr, pivotRowArr);
  if (rank == MASTER_RANK){
    free(arr);
  }
  MPI_Finalize();
  exit(EXIT_SUCCESS);
}
