#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include "gElimParallelMPIHelpers.h"

long* initArray(int N, uint seed, int p, int padding){
  if (N <= 0 || p < 0 || padding < 0) exit(EXIT_FAILURE);
  int numCols = N+1+padding;
  long* arr = calloc(N*numCols, sizeof(long));
  if (arr == NULL) {
    exit(EXIT_FAILURE);
  }
  
  srandom(seed); //set seed for random number generator
  int idx = 0;
  for (int i = 0; i < N; i++){
    for (int j = 0; j < N+1; j++){
      // Create random number from range [0;p-1] as [M,n] => M + rand() / (RAND_MAX / (N - M + 1) + 1)
      arr[idx + j] = rand() / (RAND_MAX  / p + 1);
    }
    idx += numCols;
  }
  return arr;
}

long parseArgVToLong(char* argv){
  char *endptr;
  char *str = argv;
  long argVal;
  errno = 0; /* To distringuish success/failure after call */
  argVal = strtol(argv, &endptr, 10);

  if ((errno == ERANGE && (argVal == LONG_MAX || argVal == LONG_MIN))
      || (errno != 0 && argVal == 0)) {
    // TODO: print error
    exit(EXIT_FAILURE);
  }
  if (endptr == str){
    // TODO: Print error no digits were found, see man page strtol
    exit(EXIT_FAILURE);
  }
  /* If we got here, strtol() successfully parsed a number */
  return argVal;
}

int longToInt(long val){
  if (val <= INT_MIN || val >= INT_MAX){
    // TODO: Print error
    exit(EXIT_FAILURE);
  }
  return (int)val;
}

uint longToUInt(long val){
  if (val <= INT_MIN || val >= INT_MAX){
    // TODO: Print error
    exit(EXIT_FAILURE);
  }
  return (uint)val;
}

void printArray(long* arr, int rows, int cols){
  if (rows <= 0 || cols <= 0 || arr == NULL){
    return;
  }
  for (int row = 0; row < rows; row++){
    int currRowIdx = row * cols;
    for (int col = 0; col < cols; col++){
      printf("| %ld ", arr[currRowIdx + col]);
    }
    printf("\n");
  }
}
