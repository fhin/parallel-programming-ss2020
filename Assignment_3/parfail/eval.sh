#!/bin/sh
NUM_RUNS=5
if [ $# -eq 0 ] || [ $# -gt 2 ]; then
	echo "Use script as: ./eval.sh <benchmark_file_name> <num_runs>"
	exit 1
elif [ $# -eq 2 ]; then
	NUM_RUNS=$2
fi
echo "Running benchmark with file: $BENCH_FILE"
echo "Running benchmark for $NUM_RUNS iterations"
BENCH_FILE=$1
BENCH_NAME=$(basename "$BENCH_FILE")
LOG_FILE="log_${BENCH_NAME}.txt"
RES_LOG_FILE="log_${BENCH_NAME}_results.txt"
tmp_f="tmp_f.txt"

if [ ! -f "$tmp_f" ]; then
	touch ./"$tmp_f"
else
	> ./"$tmp_f"
fi

echo "Storing complete output in log file: $LOG_FILE"
if [ ! -f "$LOG_FILE" ]; then
	touch ./"$LOG_FILE"
else
	> ./"$LOG_FILE"
fi

echo "Storing metrics in: $RES_LOG_FILE"
if [ ! -f "$RES_LOG_FILE" ]; then
	touch ./"$RES_LOG_FILE"
else
	> ./"$RES_LOG_FILE"
fi

./configure.sh && make 
echo ""
for i in 1 2 4 8 12 16 24 32 64
do
	echo "Using $i Threads"
	echo "Using $i Threads" >> ./"$RES_LOG_FILE"
	for j in $(seq $NUM_RUNS);
	do
		./parfail ${i} $BENCH_FILE | tee ./"$tmp_f" >> ./"$LOG_FILE"
		grep -E '(process time )([0-9]+).([0-9]+)\s([a-z]+)' ./"$tmp_f" >> ./"$RES_LOG_FILE"
		grep -E '(wall clock time )([0-9]+).([0-9]+)\s([a-z]+)' ./"$tmp_f" >> ./"$RES_LOG_FILE"
		grep -E '(utilization )([0-9]+)' ./"$tmp_f" >> ./"$RES_LOG_FILE"
		echo "" >> ./"$RES_LOG_FILE"
		echo "" >> ./"$LOG_FILE"
		> ./"$tmp_f"
	done
	> ./"$tmp_f"
echo "" >> ./"$RES_LOG_FILE"
echo "" >> ./"$LOG_FILE"
done
rm -f ./"$tmp_f"

