// Copyright (c) 2020 Armin Biere Johannes Kepler University Linz

#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>

// Compiler Barrier Primitives (taken from perfbook Chapter 4)
#define ACCESS_ONCE(x) (*(volatile typeof(x) *)&(x))
#define READ_ONCE(x) ({ typeof(x) ___x = ACCESS_ONCE(x); ___x; })
#define WRITE_ONCE(x, val) ({ ACCESS_ONCE(x) = (val); })
/*------------------------------------------------------------------------*/

// Messsages.

static void
error_prefix (const char *type)
{
  if (isatty (2))
    fprintf (stderr, "\033[1mparfail:\033[1;31m %s:\033[0m ", type);
  else
    fprintf (stderr, "parfail: %s: ", type);
}

static void
die (const char *fmt, ...)
{
  va_list ap;
  error_prefix ("error");
  va_start (ap, fmt);
  vfprintf (stderr, fmt, ap);
  va_end (ap);
  fputc ('\n', stderr);
  exit (1);
}

static void
msg (const char *fmt, ...)
{
  va_list ap;
 fputs ("c [parfail] ", stdout);
  va_start (ap, fmt); 
  vprintf (fmt, ap);
  va_end (ap);
  fputc ('\n', stdout);
  fflush (stdout);
}

/*------------------------------------------------------------------------*/

// Formula.

static int num_clauses;
static int num_variables;

static int **clauses;		// All clauses (zero terminated).
static int ***watches;		// Clauses watched by literal.

static int num_units;		// Number of original unit clauses.
static int *units;		// The original units found in the formula.

// Clauses are pointers to zero terminated arrays of literals. The 'watches'
// array then maps literals to a zero terminated array of pointers to
// clauses in which that literal occurs.  Note that variables are in the
// range '1..<num_variables' and literals either variables or their
// negation. After allocating 'watches' it needs to be shifted such that it
// can be accessed by negative numbers too.  Similarly negatively indexed
// arrays are 'values' and 'stamps' (as well as 'count' below).

static void
init_watches (void)
{
  unsigned *count = calloc (2 * (num_variables + 1u), sizeof *count);
  if (!count)
    die ("failed to allocated counter");
  count += num_variables;

  // First count number of occurrences of all literals.
  //
  for (int i = 0; i < num_clauses; i++)
    for (const int *p = clauses[i]; *p; p++)
      count[*p]++;

  watches = malloc (2 * (num_variables + 1u) * sizeof *watches);
  if (!watches)
    die ("failed to allocated watches");
  watches += num_variables;

  // Now allocate watcher arrays of appropriate size for each variable.
  //
  for (int lit = -num_variables; lit <= num_variables; lit++)
    if (!(watches[lit] = malloc ((count[lit] + 1u) * sizeof (int *))))
      die ("failed to allocate watches[%d]", lit);

  // Insert the clauses into the watcher arrays of each literal.
  //
  for (int i = 0; i < num_clauses; i++)
    for (const int *p = clauses[i]; *p; p++)
      *watches[*p]++ = clauses[i];

  // Add terminating zero and reset pointers back to start.
  //
  for (int lit = -num_variables; lit <= num_variables; lit++)
    *watches[lit] = 0, watches[lit] -= count[lit];

  count -= num_variables;
  free (count);
}

static void
reset_watches (void)
{
  for (int lit = -num_variables; lit <= num_variables; lit++)
    free (watches[lit]);

  watches -= num_variables;
  free (watches);
}

static void
reset_clauses (void)
{
  for (int i = 0; i < num_clauses; i++)
    free (clauses[i]);
  free (clauses);

  free (units);
}

/*------------------------------------------------------------------------*/

// Global result.

static bool inconsistent;	// Formula unsatisfiable.

static int num_learned;		// Number of learned (unit) clauses.
static int *learned;		// Stack of all learned (unit) clauses.

static pthread_mutex_t learned_mutex = PTHREAD_MUTEX_INITIALIZER; // Mutex controlling access to learned unit clauses
static pthread_mutex_t min_learned_mutex = PTHREAD_MUTEX_INITIALIZER; // Mutex used to define the min. num learned unit clauses of the current iteration

static void
new_learned_unit_clause (int unit)
{
  if (!unit)
    return;

  int absUnit = abs(unit);
  pthread_mutex_lock(&learned_mutex);
  int tmpNumLearned = READ_ONCE(num_learned);
  assert (tmpNumLearned < num_variables);
  int unitNotAssigned = 1;
  for (int i = 0; i < tmpNumLearned; i++){
    if (abs(READ_ONCE(learned[i])) == absUnit){
      unitNotAssigned = 0;
      break;
    }
  }
  if (unitNotAssigned) {
    WRITE_ONCE(learned[__sync_fetch_and_add(&num_learned, 1)], unit);
  }
  pthread_mutex_unlock(&learned_mutex);
}

static void
init_learned_unit_clauses (void)
{
  learned = calloc (num_variables, sizeof *learned);
  if (num_variables && !learned)
    die ("failed to allocate learned unit clauses");
}

/*------------------------------------------------------------------------*/

// Needed for printing and statistics at the end.  These implied literals
// are set by the simplification worker, which should have a consistent
// global assignment.  The learned units above are a subset.

static int num_implied;		// Total number of implied literals.
static int *implied;		// Stack of these implied literals.

/*------------------------------------------------------------------------*/

// Parsing.

static const char *input_path;
static FILE *input_file;
static int close_input;		// 1=fclose, 2=pclose

static bool
is_number (const char *arg)
{
  if (!*arg)
    return false;
  for (const char *p = arg; *p; p++)
    if (!isdigit (*p))
      return false;
  return true;
}

static long lineno = 1;

static int
read_next_char (void)
{
  int res = getc (input_file);
  if (res == '\n')
    lineno++;
  return res;
}

static void
parse_error (const char *fmt, ...)
{
  va_list ap;
  error_prefix ("parse error");
  fprintf (stderr, " in '%s' line %ld: ", input_path, lineno);
  va_start (ap, fmt);
  vfprintf (stderr, fmt, ap);
  va_end (ap);
  fputc ('\n', stderr);
  exit (1);
}

static void
parse (void)
{
  int ch;
  for (;;)
    {
      ch = read_next_char ();
      if (ch == 'c')
	{
	  while ((ch = read_next_char ()) != '\n')
	    if (ch == EOF)
	      parse_error ("end-of-file in comment");
	}
      else if (ch == 'p')
	break;
      else
	parse_error ("expected 'p' or 'c'");
    }

  if (read_next_char () != ' ' ||
      read_next_char () != 'c' ||
      read_next_char () != 'n' ||
      read_next_char () != 'f' || read_next_char () != ' ')
    parse_error ("expected 'p cnf '");
  if (!isdigit (ch = read_next_char ()))
    parse_error ("expected digit after 'p cnf '");
  num_variables = ch - '0';
  while (isdigit (ch = read_next_char ()))
    {
      if (INT_MAX / 10 < num_variables)
	parse_error ("number of variables too large");
      num_variables *= 10;
      const int digit = ch - '0';
      if (INT_MAX - digit < num_variables)
	parse_error ("number of variables too large");
      num_variables += digit;
    }
  if (ch != ' ')
    parse_error ("expected space after 'p cnf %d'", num_variables);
  if (!isdigit (ch = read_next_char ()))
    parse_error ("expected digit after 'p cnf %d '");
  num_clauses = ch - '0';
  while (isdigit (ch = read_next_char ()))
    {
      if (INT_MAX / 10 < num_clauses)
	parse_error ("number of clauses too large");
      num_clauses *= 10;
      const int digit = ch - '0';
      if (INT_MAX - digit < num_clauses)
	parse_error ("number of clauses too large");
      num_clauses += digit;
    }
  if (ch != '\n')
    parse_error ("expected new line after 'p cnf %d %d'",
		 num_variables, num_clauses);
  msg ("found 'p cnf %d %d' header", num_variables, num_clauses);

  clauses = malloc (num_clauses * sizeof *clauses);
  if (num_clauses && !clauses)
    die ("failed to allocate clauses");

  units = malloc (num_clauses * sizeof *units);
  if (num_clauses && !units)
    die ("failed to allocate original units");

  int *clause = 0;
  size_t allocated = 0;
  size_t size = 0;

  int parsed = 0;
  int lit = 0;

  for (;;)
    {
      ch = read_next_char ();
      if (ch == ' ' || ch == '\t' || ch == '\n')
	continue;
      if (ch == 'c')
	{
	  while ((ch = read_next_char ()) != '\n')
	    if (ch == EOF)
	      parse_error ("end-of-file in comment");
	  continue;
	}
      if (ch == EOF)
	{
	  if (parsed < num_clauses)
	    parse_error ("clause missing");
	  if (lit)
	    parse_error ("terminating zero missing");
	  break;
	}
      int sign = 1;
      if (ch == '-')
	{
	  sign = -1;
	  ch = read_next_char ();
	  if (!isdigit (ch))
	    parse_error ("expected digit after '-'");
	}
      else if (!isdigit (ch))
	parse_error ("expected literal");
      if (parsed == num_clauses)
	parse_error ("too many clauses");
      lit = ch - '0';
      while (isdigit (ch = read_next_char ()))
	{
	  if (INT_MAX / 10 < lit)
	    parse_error ("literal too large");
	  lit *= 10;
	  const int digit = ch - '0';
	  if (INT_MAX - digit < lit)
	    parse_error ("literal too large");
	  lit += digit;
	}
      if (lit > num_variables)
	parse_error ("literal too large");
      lit *= sign;
      assert (lit <= num_variables);
      if (ch != ' ' && ch != '\t' && ch != '\n')
	parse_error ("expected space after '%d'", lit);
      if (lit)
	{
	  if (size == allocated)
	    {
	      allocated = allocated ? 2 * allocated : 1;
	      clause = realloc (clause, allocated * sizeof *clause);
	      if (!clause)
		die ("failed to reallocate clause");
	    }
	  clause[size++] = lit;
	}
      else
	{
	  if (!(clauses[parsed] = malloc ((size + 1) * sizeof (int))))
	    die ("failed allocate clause");
	  memcpy (clauses[parsed], clause, size * sizeof (int));
	  clauses[parsed][size] = 0;
	  if (!size)
	    inconsistent = true;
	  if (size == 1)
	    units[num_units++] = clause[0];
	  parsed++;
	  size = 0;
	}
    }
  free(clause);

  msg ("found %d original unit clauses", num_units);
}

/*------------------------------------------------------------------------*/

// Threading.

static int num_threads;

static void
init_threads (void)
{
  if (num_threads)
    msg ("using %d threads as specified", num_threads);
  else if ((num_threads = sysconf (_SC_NPROCESSORS_ONLN)) > 0)
    msg ("using %d threads as determined by 'sysconf'", num_threads);
  else
    {
      num_threads = 1;
      msg ("using 1 thread (since 'sysconf' failed)");
    }
}

/*------------------------------------------------------------------------*/


// Statistics.

static double started;

static double
wall_clock_time (void)
{
  double res = 0;
  struct timeval tv;
  if (!gettimeofday (&tv, 0))
    res = 1e-6 * tv.tv_usec, res += tv.tv_sec;
  return res;
}

static double
process_time (void)
{
  struct rusage u;
  if (getrusage (RUSAGE_SELF, &u))
    return 0;
  double res = u.ru_utime.tv_sec + 1e-6 * u.ru_utime.tv_usec;
  res += u.ru_stime.tv_sec + 1e-6 * u.ru_stime.tv_usec;
  return res;
}

static long propagations;
static long decisions;
static long conflicts;

static double
average (double a, double b)
{
  return b ? a / b : 0;
}

static double
percent (double a, double b)
{
  return average (100 * a, b);
}

static void
statistics (void)
{
  double w = wall_clock_time () - started;
  double p = process_time ();

  msg ("");
  msg ("propagations %ld (%.2f millions per second)",
       propagations, average (propagations * 1e-6, w));
  msg ("decisions %ld (%.1f propagations per decisions)",
       decisions, average (propagations, decisions));
  msg ("conflicts %ld (%.1f decisions per conflict)",
       conflicts, average (conflicts, decisions));
  msg ("learned %d (%.0f%% learned unit clauses per conflict)",
       num_learned, percent (num_learned, conflicts));
  msg ("implied %d (%.1f per learned unit clause)",
       num_implied, average (num_implied, num_learned));
  msg ("");
  msg ("process time %.3f seconds", w);
  msg ("wall clock time %.3f seconds", w);
  msg ("utilization %.0f%% for %d threads",
       percent (p, w * num_threads), num_threads);
}

/*------------------------------------------------------------------------*/

// Probing.

typedef struct Worker Worker;

struct Worker
{
  int *trail;			// stack of assigned literals
  int assigned;			// number of literals on trail
  int propagated;		// next literal on trail to propagate
  unsigned *stamps;		// 'num_learned' when last propagated
  signed char *values;		// -1=false, 0=unassigned, 1=true
  int remainingLiteralCnt;
  int literalIntervalLen;
  int literalIntervalDist;
  int *literals;
  int *literals_backup;
  int localStamp;
  int middleOffset;
  int *middle;
};

static Worker *
new_worker ()
{
  Worker *worker = malloc (sizeof *worker);
  if (!worker)
    die ("failed to allocate worker");

  worker->trail = malloc (num_variables * sizeof *worker->trail);
  if (num_variables && !worker->trail)
    die ("failed to allocate trail");

  worker->assigned = worker->propagated = 0;

  worker->values = calloc (2 * (num_variables + 1u), sizeof *worker->values);
  if (!worker->values)
    die ("failed to allocate values");
  worker->values += num_variables;

  worker->stamps = calloc (2 * (num_variables + 1u), sizeof *worker->stamps);
  if (!worker->stamps)
    die ("failed to allocate stamps");
  worker->stamps += num_variables;
  worker->localStamp = 0;
  return worker;
}

static Worker*
assign_literals_worker(Worker *worker, int numLiteralsToCheck, int start_literal_idx)
{
  worker->literalIntervalLen = 2*numLiteralsToCheck;
  worker->remainingLiteralCnt = 2*numLiteralsToCheck;
  worker->literalIntervalDist = numLiteralsToCheck;
  worker->middleOffset = 0;
  worker->literals = calloc (2*numLiteralsToCheck, sizeof(int));  
  if (!worker->literals){
    die ("failed to allocate literals for worker");
  }
  worker->middle = worker->literals + numLiteralsToCheck-1;
  worker->literals_backup = &worker->literals[0];

  int start_num = num_variables - start_literal_idx;
  int endIdx = worker->literalIntervalLen-1;
  for (int i = 0; i < numLiteralsToCheck; i++){
    worker->literals[i] = -start_num;
    worker->literals[endIdx--] = start_num--;
  }
  return worker;
}

static void
delete_worker (Worker * worker)
{
  worker->values -= num_variables;
  free (worker->values);

  worker->stamps -= num_variables;
  free (worker->stamps);

  free (worker->trail);
  if (worker->literals)
    free (worker->literals);
  
  free (worker);
}

static void
assign (Worker * worker, int lit)
{
  assert (!worker->values[lit]);
  assert (!worker->values[-lit]);
  worker->values[lit] = 1;
  worker->values[-lit] = -1;
  assert (worker->assigned < num_variables);

  // Saved assigned literal on 'trail', which acts as a working queue to
  // propagate literals through the 'propagate' offset up to 'assigned'.
  //
  worker->trail[worker->assigned++] = lit;
}

static void
unassign (Worker * worker, int lit)
{
  assert (worker->values[lit] > 0);
  assert (worker->values[-lit] < 0);
  worker->values[lit] = worker->values[-lit] = 0;
}

static bool
propagate (Worker * worker)
{
  int saved = worker->propagated;

  while (worker->propagated < worker->assigned)
    {
      const int lit = worker->trail[worker->propagated++];
      assert (worker->values[lit] > 0);

      // Now go through all clauses with '-lit'.
      //
      int *clause;
      for (int **w = watches[-lit]; (clause = *w); w++)
	{
	  int unassigned = 0;
	  int satisfied = 0;
	  int other = 0;
	  int unit = 0;

	  for (const int *p = clause; (other = *p); p++)
	    {
	      const signed char value = worker->values[other];

	      if (value < 0)	// Skip falsified literals.
		continue;

	      if (value > 0)	// This clause is satisfied.
		{
		  satisfied++;
		  break;
		}

	      assert (!value);
	      if (unassigned++)
		break;		// Found two unassigned literals>

	      unit = *p;
	    }

	  if (satisfied)
	    continue;

	  if (unassigned > 1)
	    continue;

	  if (!unassigned)
	    return false;	// Found conflict since all falsified.

	  assert (unassigned == 1);
	  assert (unit);

	  assign (worker, unit);
	}
    }
  // Update to correctly upate in parallel execution (e.g. atomic operations, etc.)
  __sync_fetch_and_add(&propagations, worker->propagated - saved);

  return true;
}

static int
probe_literal (Worker * worker, int probe)
{
  assert (probe);

  if (worker->values[probe])
    return 1;

  // Check if since the last time this 'probe' was propagated and did not
  // lead to a conflict a new unit was found. If not, no need to propagate.
  //
  //const unsigned stamp = READ_ONCE(num_learned) + 1u;
  const unsigned stamp = worker->localStamp + 1u;
  if (worker->stamps[probe] >= stamp)
    return 1;

  const int saved = worker->assigned;
  assign (worker, probe);
  __sync_add_and_fetch(&decisions, 1);

  if (propagate (worker))	// Propagation does not lead to a conflict.
    {
      // Backtrack to previous state by unassigning the probing literal
      // 'probe' and all the implied literals.  Mark all those literals as
      // having been tried until the current number of learned unit clauses.
      while (saved < worker->assigned)
	{
	  const int other = worker->trail[--worker->assigned];
	  unassign (worker, other);

	  // Remember that for the number of learned unit clauses found so
	  // far this other literal has been propagated and did not lead to
	  // a conflict.
	  //
	  worker->stamps[other] = stamp;
	}
      worker->propagated = worker->assigned;
      return 1;
    }
  else				// Propagation lead to a conflict.
    {
      __sync_add_and_fetch(&conflicts, 1);

      // First backtrack (without time stamping).
      //
      while (saved < worker->assigned)
	unassign (worker, worker->trail[--worker->assigned]);
      worker->propagated = worker->assigned;

      // Save resulting learned unit clause (but not all the implied
      // literals derived in the following propagation).
      //
      const unsigned unit = -probe;
      new_learned_unit_clause (unit);

      // Assign negation of failed literal locally and propagate it.
      //
      assign (worker, unit);
      if (!propagate (worker))
	WRITE_ONCE(inconsistent, true);	// Mark formula as being unsatisfiable.
      return 0;
    }
}

static void
propagate_original_units (Worker * worker)
{
  for (int i = 0; !inconsistent && i < num_units; i++)
    {
      int unit = units[i];
      const signed char value = worker->values[unit];
      if (value > 0)
	continue;
      if (value < 0)
	inconsistent = true;
      if (!value)
	assign (worker, unit);
    }
  if (!propagate (worker))
    inconsistent = true;
}

/*------------------------------------------------------------------------*/

// Simplifying.

static void
simplify_clauses (Worker * worker)
{
  int remain = 0;
  for (int i = 0; i < num_clauses; i++)
    {
      int *clause = clauses[i];
      bool satisfied = false;
      int *q = clause, lit;
      for (const int *p = q; !satisfied && (lit = *p); p++)
	{
	  const signed char value = worker->values[lit];
	  if (value > 0)
	    satisfied = true;	// Found true literal.
	  else if (!value)
	    *q++ = lit;		// Only keep unassigned literals.
	}

      if (satisfied)
	free (clause);
      else
	{
	  clauses[remain++] = clause;
	  *q = 0;
	}
    }
  msg ("removed %d clauses (%d remain %.0f%%)",
       num_clauses - remain, remain, percent (remain, num_clauses));
  num_clauses = remain;

  implied = malloc (worker->assigned * sizeof *implied);
  if (worker->assigned && !implied)
    die ("failed to allocate implied literals");

  for (int idx = 1; idx <= num_variables; idx++)
    {
      const signed char value = worker->values[idx];
      if (value)
	implied[num_implied++] = value * idx;
    }
  assert (num_implied == worker->assigned);
}

/*------------------------------------------------------------------------*/

// Printing.

static FILE *output_file;
static const char *output_path;

static void
print (void)
{
  FILE *file = output_file ? output_file : stdout;
  if (inconsistent)
    fprintf (file, "p cnf %d 1\n0\n", num_variables);
  else
    {
      fprintf (file, "p cnf %d %d\n",
	       num_variables, num_implied + num_clauses);
      for (int i = 0; i < num_implied; i++)
	fprintf (file, "%d 0\n", implied[i]);
      for (int i = 0; i < num_clauses; i++)
	{
	  for (const int *p = clauses[i]; *p; p++)
	    fprintf (file, "%d ", *p);
	  fprintf (file, "0\n");
	}
    }
}

/*------------------------------------------------------------------------*/

// Compressed file reading.

static bool
has_suffix (const char *str, const char *suffix)
{
  size_t l = strlen (str), k = strlen (suffix); 
  return k <= l && !strcmp (str + l - k, suffix);
}

static void
open_compressed_input (const char *fmt)
{
  assert (input_path);
  char *cmd = malloc (strlen (fmt) + strlen (input_path));
  sprintf (cmd, fmt, input_path);
  input_file = popen (cmd, "r");
  free (cmd);
  if (!input_file)
    die ("can not read compressed '%s'", input_path);
  close_input = 2;
}

/*------------------------------------------------------------------------*/

static Worker **workers;
static pthread_t *worker_threads;
static pthread_barrier_t worker_sync_barrier;
static int new_unit_set = 0;

typedef struct RunArg RunArg;
struct RunArg
{
  Worker *worker;
  int start_idx;
  int literalsAssigned;
};

static int
update_literals(Worker *worker, int prev_num_assigned, int curr_num_assigned)
{
  if (!(curr_num_assigned - prev_num_assigned))
    return 0;
  
  int lit, absLit, trim = 0, absLitStart, absLitEnd;
  for (int trailIdx = prev_num_assigned; trailIdx < curr_num_assigned; trailIdx++){
    lit = worker->trail[trailIdx];
    absLit = abs(lit);
    absLitStart = abs(*worker->literals);
    absLitEnd = abs(*(worker->middle - worker->middleOffset));

    if (absLitStart && absLitEnd)
      if (absLit > absLitStart || absLit < absLitEnd){
	continue;
      }
    
    if (absLit == absLitStart){
      worker->literals++;
      trim++;
      worker->remainingLiteralCnt -= 2;
      worker->literalIntervalLen -= 2;
      worker->literalIntervalDist--;
    }
    else if (absLit == absLitEnd){
      worker->remainingLiteralCnt -= 2;
      *(worker->middle - worker->middleOffset) = 0;
      worker->middleOffset++;
      *(worker->middle + worker->middleOffset) = 0;
      worker->literalIntervalDist--;
    }
    else {
      for (int j = 0; j < worker->literalIntervalDist; j++){
	if (absLit == abs(worker->literals[j])){
	  worker->literals[j] = worker->literals[worker->literalIntervalLen - j - 1] = 0;
	  worker->remainingLiteralCnt -= 2;
	  break;
	}
      }
    }
  }
  if (!worker->remainingLiteralCnt)
    return trim;
  
  int intervalLen = worker->literalIntervalDist;
  for (int i = 0; i < intervalLen; i++){
    if (*worker->literals)
      break;
    worker->literals++;
    worker->literalIntervalLen -= 2;
    worker->literalIntervalDist--;
    trim++;
  }
  return trim;
}

static RunArg*
new_run_arg(Worker *worker, int start_literal_idx, int literalsAssigned)
{
  RunArg *arg = malloc(sizeof *arg);
  if (!arg){
    die ("failed to created argument for failed literal run");
  }
  arg->worker = worker;
  arg->start_idx = start_literal_idx;
  arg->literalsAssigned = literalsAssigned;
  return arg;
}

static void *
run_failed_literal_probing(void *ptr)
{
  RunArg *arg = ptr;
  arg->worker = assign_literals_worker(arg->worker, arg->literalsAssigned, arg->start_idx);
  Worker *worker = arg->worker;
  
  int prev_num_assigned = 0, curr_lit_ok = 1, curr_learned_idx = 0, curr_num_learned = 0, num_learned_start = 0;
  int learned_new = 0, prev_learned_idx = -1, curr_lit_idx = 0;

  propagate_original_units(worker);
  if (READ_ONCE(inconsistent))
    return 0;
  update_literals(worker, prev_num_assigned, worker->assigned);
  
  do {
    int lit = 0;
    if (learned_new){
      num_learned_start++;
    }
    // Probe all assigned literals
    if (worker->remainingLiteralCnt > 0) {
      int elemsToCheck = worker->literalIntervalLen;
      prev_num_assigned = worker->assigned;
      
      do {
	lit = *(worker->literals + (curr_lit_idx++));
	curr_lit_ok = lit ? probe_literal(worker, lit) : 1;

	// Check if new failed literal was found
	if (READ_ONCE(num_learned) > curr_num_learned || !curr_lit_ok){
	  curr_lit_idx--;
	  break;
	}
	if (curr_lit_idx == worker->literalIntervalLen)
	  curr_lit_idx = 0;
      } while (--elemsToCheck > 0);
    }
    pthread_barrier_wait(&worker_sync_barrier);
    learned_new = !(prev_learned_idx == (READ_ONCE(num_learned)-1));
    if (READ_ONCE(new_unit_set)){
      WRITE_ONCE(new_unit_set, 0);
    }
    pthread_barrier_wait(&worker_sync_barrier);
    if (!learned_new)
      break;
    
    const int learnedUnit = READ_ONCE(learned[curr_learned_idx++]);
    // Update number of learned failed literals
    pthread_mutex_lock(&min_learned_mutex);
    if (!READ_ONCE(new_unit_set)){
      WRITE_ONCE(num_learned, curr_learned_idx);
      WRITE_ONCE(new_unit_set, 1);
    }
    pthread_mutex_unlock(&min_learned_mutex);
    prev_learned_idx = curr_learned_idx-1;
    worker->localStamp = curr_num_learned = curr_learned_idx;

    if (learnedUnit != -lit){
      // Unassign literals if newly learned failed literal is different than the one found by this worker
      if (!curr_lit_ok && lit) {
	while (prev_num_assigned < worker->assigned){
	  unassign (worker, worker->trail[--worker->assigned]);
	}
	worker->propagated = prev_num_assigned;
      }
      assign (worker, learnedUnit);
      if (!propagate(worker)){
	WRITE_ONCE(inconsistent, true);
      }
    }
    // Remove literals that were assigned by propagating the new failed literal
    int trim = update_literals(worker, prev_num_assigned, worker->assigned);
    if (trim) {
      if (curr_lit_idx)
	curr_lit_idx -= trim + (learnedUnit == -lit ? 1 : 0);
	
      if (curr_lit_idx < 0 || curr_lit_idx >= worker->literalIntervalLen)
	curr_lit_idx = 0;
      continue;
    }
    else {
      if (++curr_lit_idx >= worker->literalIntervalLen){
	curr_lit_idx = 0;
      }
    }
    curr_lit_ok = 1;
  } while (!READ_ONCE(inconsistent) && READ_ONCE(num_learned) > num_learned_start);
  free(arg);
  return 0;
}

static void
init_and_run_workers()
{
  if (!num_variables){
    Worker *worker = new_worker();
    propagate_original_units(worker);
    if (!inconsistent)
      simplify_clauses(worker);
    free(worker);
    return;
  }
  int literals_per_thread = num_variables / num_threads;
  int leftover_literals = !literals_per_thread ? num_variables : num_variables % literals_per_thread;
  int start_literal_idx = 0;

  if (!literals_per_thread){
    num_threads = leftover_literals;
    msg ("Reduced num threads to %u", leftover_literals);
  }
  
  if (pthread_barrier_init(&worker_sync_barrier, 0, num_threads)){
    die ("failed to initalize worker sync barrier");
  }
  
  workers = calloc(num_threads, sizeof *workers);
  worker_threads = calloc(num_threads, sizeof worker_threads);
  if (!workers){
    die ("Failed to allocate workers");;
    return;
  }
  if (!worker_threads){
    die ("Failed to allocate resources for worker threads");
    free(workers);
    return;
  }
  
  int literalsAssigned;
  for (int i = 0; i < num_threads; i++){
    literalsAssigned = leftover_literals-- > 0 ? literals_per_thread + 1 : literals_per_thread;
    *(workers+i) = new_worker();
    RunArg *arg = new_run_arg(*(workers+i), start_literal_idx, literalsAssigned);
    if (pthread_create (&worker_threads[i], 0, run_failed_literal_probing, arg))
      die ("failed to create worker thread %u\n", i);
    start_literal_idx += literalsAssigned;
  }

  for (int i = 0; i < num_threads; i++){
    if (pthread_join (worker_threads[i], 0)){
      die ("failed to join worker thread %u\n", i);
    }
    workers[i]->literals = workers[i]->literals_backup;
  }

  simplify_clauses(*workers);
  
  // Cleanup
  for (int i = 0; i < num_threads; i++){
    delete_worker(*(workers+i));
  }
  
  if (pthread_barrier_destroy(&worker_sync_barrier)){
    die ("failed to destroy worker sync barrier");
  }
  free(workers);
  free(worker_threads);
  return;
}

int
main (int argc, char **argv)
{
  started = wall_clock_time ();
  for (int i = 1; i < argc; i++)
    {
      const char *arg = argv[i];
      if (!strcmp (arg, "-h"))
	{
	  printf ("usage: parfail [-h] [<threads>] [<input> [<output>]]\n");
	  exit (0);
	}
      else if (arg[0] == '-' && arg[1])
	die ("invalid option '%s' (try '-h')", arg);
      else if (is_number (arg))
	{
	  if (num_threads)
	    die ("multiple thread options '%d' and '%s'", num_threads, arg);
	  if ((num_threads = atoi (arg)) <= 0)
	    die ("invalid number of threads '%s'", arg);
	}
      else if (output_path)
	die ("too many files '%s' and '%s'", input_path, output_path);
      else if (!input_path)
	{
	  if (!strcmp (arg, "-"))
	    die ("invalid input path '-'");
	  input_path = arg;
	}
      else
	output_path = arg;
    }
  if (!input_path)
    input_path = "<stdin>", input_file = stdin;
  else if (has_suffix (input_path, ".gz"))
    open_compressed_input ("gzip -c -d %s");
  else if (has_suffix (input_path, ".bz2"))
    open_compressed_input ("bzip2 -c -d %s");
  else if (has_suffix (input_path, ".xz"))
    open_compressed_input ("xz -c -d %s");
  else if (!(input_file = fopen (input_path, "r")))
    die ("can not read '%s'", input_path);
  else
    close_input = 1;
  msg ("Parfail Failed Literal Preprocessor");
  msg ("");
  msg ("reading DIMACS file from '%s'", input_path);
  parse ();
  if (close_input == 1)
    fclose (input_file);
  if (close_input == 2)
    pclose (input_file);

  init_learned_unit_clauses ();
  init_watches ();
  init_threads ();
  init_and_run_workers();
  reset_watches();

  if (inconsistent)
    msg ("formula UNSATISFIABLE (inconsistent)");
  else if (num_implied == num_variables)
    msg ("formula SATISFIABLE (all variables assigned)");
  else
    msg ("formula not solved");

  msg ("");

  if (output_path && !strcmp (output_path, "-"))
    {
      msg ("writing simplified formula to '<stdout>'");
      print ();
    }
  else if (!output_path)
    msg ("not writing simplified CNF (use '-' to write to '<stdout>')");
  else if (!(output_file = fopen (output_path, "w")))
    die ("can not write to '%s'", output_path);
  else
    {
      msg ("writing simplified formula to '%s'", output_path);
      print ();
      fclose (output_file);
    }
  reset_clauses ();
  free (learned);
  free (implied);
  statistics ();
  return 0;
}
