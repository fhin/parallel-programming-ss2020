#!/bin/sh

cd `dirname $0`/..

if [ -t 2 ]
then
  BOLD="\e[1m"
  NORMAL="\e[0m"
  RED="\e[1;31m"
else
  BOLD=""
  NORMAL=""
  RED=""
fi

die () {
  echo "${BOLD}test.sh:${RED} error:${NORMAL} $*" 1>&2
  exit 1
}
[ $# = 0 ] && die "expected at least one CNF as argument"

parfail=./parfail
[ -x $parfail ] || die "can not execute '$parfail'"

run () {
  input=$1
  [ -f $input ] || die "can not read '$input'"
  expected=`awk '/^c implied /{print $3}' $input`
  [ "$expected" = "" ] && die "no 'c units <n>' comment in '$input'"
  base=`echo $input|sed -e 's,.cnf$,,'`
  output=$base.out
  log=$base.log
  cmd="$parfail $input $output"
  rm -f $log $output
  echo "$cmd > $log"
  $cmd > $log || die "non zero exit code"
  found="`awk '/c .parfail. implied /{print $4}' $log`"
  test "$found" = "$expected" || \
  die "found '$found' units but expected '$expected'"
}

for input in $*
do
  run $input
done
