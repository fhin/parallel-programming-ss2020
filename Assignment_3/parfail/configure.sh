#!/bin/sh
debug=no
die () {
  echo "configure: error: $*" 1>&2
  exit 1
}
usage () {
cat <<EOF
usage: configure.sh [-h|--help] [-g|--debug]
EOF
}
while [ $# -gt 0 ]
do
  case "$1" in
    -h|--help) usage; exit 0;;
    -g|--debug) debug=yes;;
    *) die "invalid option '$1' (try '-h')";;
  esac
  shift
done
COMPILE="gcc -W -Wall -ggdb3 -pthread"
[ $debug = no ] && COMPILE="$COMPILE -O3 -DNDEBUG"
echo "$COMPILE"
sed -e "s,@COMPILE@,$COMPILE," makefile.in > makefile
