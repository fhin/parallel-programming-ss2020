#!/bin/sh
num_runs=1
if [ $# -eq 0 ] || [ $# -gt 2 ]; then
	echo "Use script as: ./eval_general.sh <exec_file_name> <num_runs>"
	exit 1
elif [ $# -eq 2 ]; then
	num_runs=$2
fi
EXEC_FILE=$1
LOG_FILE="log_${EXEC_FILE}.txt"

echo "Using log file: $LOG_FILE"
if [ ! -f "$LOG_FILE" ]; then
	touch ./"$LOG_FILE"
else
	> ./"$LOG_FILE"
fi

for i in 1 2 3 4
do
	for j in $(seq $num_runs);
	do
		echo "Using $i workers"
		./configure.sh && make && ./"$EXEC_FILE" $i 9 >> ./"$LOG_FILE"
		wait
	done
	echo ""
	echo "" >> ./"$LOG_FILE"
done

