#!/bin/sh
FILE_ONE="log_bogus_one.txt"
FILE_TWO="log_bogus_two.txt"
EXEC_FILE="bogus"

echo "Using log file $FILE_ONE"
if [ ! -f "$FILE_ONE" ]; then
	touch ./"$FILE_ONE"
else
	> ./"$FILE_ONE"
fi
echo "Using 1 worker"
./configure.sh && make && ./"$EXEC_FILE" 1 9 >> ./"$FILE_ONE"
./configure.sh -g && make && ./"$EXEC_FILE" 1 9 >> ./"$FILE_ONE"

echo "Using log file $FILE_TWO"
if [ ! -f "$FILE_TWO" ]; then
	touch ./"$FILE_TWO"
else
	> ./"$FILE_TWO"
fi
echo "Using 2 workers"
./configure.sh && make && ./"$EXEC_FILE" 2 9 >> ./"$FILE_TWO"
./configure.sh -g && make && ./"$EXEC_FILE" 2 9 >> ./"$FILE_TWO"
